Ferm iptables configuration
===========================

Ansible role to install `ferm` and configure iptables rulesets.


Variables
---------


Files
-----

* `ferm.conf`:
  The iptables rule file for ferm.


References
----------

* [ferm](http://ferm.foo-projects.org/)
